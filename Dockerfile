FROM node:14-alpine

WORKDIR /app
COPY . /app

RUN apk --no-cache add \
      bash \
      g++ \
      ca-certificates \
      lz4-dev \
      musl-dev \
      cyrus-sasl-dev \
      openssl-dev \
      make \
      python

RUN apk add --no-cache --virtual .build-deps gcc zlib-dev libc-dev bsd-compat-headers py-setuptools bash


RUN yarn global add tsc \
    && yarn global add concurrently \
    && yarn global add typescript

ENV WITH_SASL 0

RUN yarn install --silent
RUN yarn run build
RUN apk del .build-deps

# Variables
ARG PORT
ENV PORT  ${PORT:-3000}

EXPOSE ${PORT:-3000}

CMD ["yarn", "run", "prod:worker"]






