declare namespace NodeJS {
  interface Global {
    kafka: import('@shared/kafka').default
      // @ts-ignore
      producer: import('node-rdkafka').Producer,
    produce: Function
  }
}
