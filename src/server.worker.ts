import 'module-alias/register'
import http from 'http'
import express from 'express'
import terminus from '@godaddy/terminus'
import bodyParser from 'body-parser'
import KafkaHelper from '@shared/kafka'
import * as User from '@handlers/user'
import { Database } from '@db'

import debugjs from 'debugjs-wrapper'
const { debug } = debugjs.all('gateway:worker')
const PORT = process.env.PORT ? process.env.PORT : 3000


const startServer = async () => {

  await Database.connect()

  global.kafka = new KafkaHelper()
  global.kafka.init({
    brokerList: process.env.BROKER_LIST ? process.env.BROKER_LIST :'127.0.0.1:9092',
    groupId: process.env.CONSUMER_GROUP ? process.env.CONSUMER_GROUP : 'gateway-kjs'
  })

  global.kafka.subscribe('hey.topic', (params) => {
    debug(params)
    console.log(params)
  })




  const healthCheck = {
    logger: console.log,
    healthChecks: {
      '/healthz': () => Promise.resolve()
    }
  }

  const app = express()
  app.use(bodyParser.json())

  app.post('/user.save', (req, res) => User.save(req, res))
  app.post('/test.kafka', (req, res) => {
    global.kafka.produce('hey.topic', {someData: 'data'}, null)
    res.status(200).send('Good')
  })

  const server = http.createServer(app)
  terminus(server, healthCheck).listen(PORT, () => {
    console.log('listening at %s', PORT)
  })

}

startServer()