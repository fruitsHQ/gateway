export const forEach = (array, callback) => {
  for(let i = 0; i < array.length; i++) {
    const item = array[i]
    callback(item, i, array)
  }
}

export const breakableForEach = (array, callback) => {
  for(let i = 0; i < array.length; i++) {
    const item = array[i]
    const shouldBreak = callback(item, i, array)

    if(shouldBreak) {
      break
    }
  }
}

export const find = (array, callback) => {
  let output

  for(let i = 0; i < array.length; i++) {
    const item = array[i]
    const isSuccess = callback(item, i, array)

    if(isSuccess) {
      output = item
      break
    }
  }

  return output
}

export const map = (array, callback) => {
  const output = []

  for(let i = 0; i < array.length; i++) {
    const item = array[i]
    const result = callback(item, i, array)

    output.push(result)
  }

  return output
}

export const reduce = (array, callback, acc?: any) => {
  let accumulator = acc ?? array[0]
  const startIndex = (acc !== null && acc !== undefined)
    ? 0
    : 1

  for(let i = startIndex; i < array.length; i++) {
    const item = array[i]
    const result = callback(accumulator, item, i, array)

    accumulator = result
  }

  return accumulator
}

export const filter = (array, callback) => {
  const output = []

  for(let i = 0; i < array.length; i++) {
    const item = array[i]
    const isSuccess = callback(item, i, array)

    if(isSuccess) {
      output.push(item)
    }
  }

  return output
}

export const some = (array, callback): boolean => {
  let output = false

  for (let i = 0; i < array.length; i++) {
    const item = array[i]
    const isSuccess = callback(item, i, array)

    if (isSuccess) {
      output = true

      break
    }
  }

  return output
}

export const every = (array, callback): boolean => {
  let output = true

  for (let i = 0; i < array.length; i++) {
    const item = array[i]
    const isSuccess = callback(item, i, array)

    if (!isSuccess) {
      output = false

      break
    }
  }

  return output
}
