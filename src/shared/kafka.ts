// @ts-ignore
import Kafka from 'node-rdkafka'

import debugjs from 'debugjs-wrapper'
const { debug, error } = debugjs.all('gateway:shared:kafka')

class KafkaHelper {
  private consumer
  private producer
  private client
  private subscribers = new Map()

  public init(conf){

    this.client = Kafka.AdminClient.create({
      'client.id': conf.clientId,
      'metadata.broker.list': conf.brokerList
    })

    this.consumer = new Kafka.KafkaConsumer({
      'group.id': conf.groupId,
      'metadata.broker.list': conf.brokerList
    }, {})

    this.producer = new Kafka.Producer({
      'metadata.broker.list': conf.brokerList,
      'dr_cb': true
    })

    this.consumer
      .on('ready', () => {
        console.log(this.subscribers.keys())
        this.consumer.subscribe(Array.from(this.subscribers.keys()))
        this.consumer.consume()
      })
      .on('data', msg => {
        const handler = this.subscribers.get(msg.topic)
        handler(msg.value.toString())
      })

    this.producer.on('event.error', (err) => {
      error('Error from producer')
      error(err)
    })


    this.producer.setPollInterval(100)
    this.producer.connect()
    this.consumer.connect()

  }

  public subscribe(topic, handler){
    this.subscribers.set(topic, handler)
    console.log('Creating new topic')
    this.client.createTopic({
      topic: topic,
      num_partitions: 1,
      replication_factor: 1
    }, (err) => {
      if(err) {
        error(err.message)
      } else {
        debug('Created new topic', topic)
      }
    })
  }

  public produce(topic, params, key){
    try {
      this.producer.produce(
        topic,
        null,
        Buffer.from(JSON.stringify(params)),
        key,
        Date.now(),
        'someToken'
      )
    } catch (err) {
      error('A problem occurred when sending our message')
      error(err)
    }
  }
}
export default KafkaHelper