import debugjs from 'debugjs-wrapper'
import { User } from '@models/user'

const { debug, error } = debugjs.all('core:handlers:user')

export const save = async (req, res) => {

  try{
    const {
      userId
    } = req.body

    await User.findOneAndUpdate({
      userId
    }, req.body, {upsert: true}).lean().exec()

    debug('User saved!')
    res.status(200).send('Saved')
  }catch(e){
    error('User cannot be saved', e.message)
    res.status(404).send(e.message)
  }


}