import mongoose, { Schema } from 'mongoose'




const ActorSchema = new Schema({
  userId: { type: String, required: true, index: true, unique: true },
  userName: { type: String }
})


export const User = mongoose.model('User', ActorSchema)


