import mongoose from 'mongoose'
import debugjs from 'debugjs-wrapper'
const { debug } = debugjs.all('db:worker')


export class Database {
  static async connect() {

    debug('Connecting  to***', process.env.MONGODB_URI)
    try {
      await mongoose.connect(
        process.env.MONGODB_URI,
        {
          reconnectTries: Number.MAX_VALUE,
          useNewUrlParser: true
        }
      )

      debug('Connected to mongo DB')
    } catch (err) {
      console.error('Failed to connect to mongo DB', err)
    }

  }
}
